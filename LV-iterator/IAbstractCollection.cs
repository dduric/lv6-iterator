﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_iterator
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
