﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV_iterator
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook nb = new Notebook();

            nb.AddNote(new Note("123", "djasjkdh"));
            nb.AddNote(new Note("456", "asdnsa"));

            IAbstractIterator iterator = nb.GetIterator();

            do
            {
                iterator.Current.Show();
            } while (iterator.Next() != null);
        }
    }
}
